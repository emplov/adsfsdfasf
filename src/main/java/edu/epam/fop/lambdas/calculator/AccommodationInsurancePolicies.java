package edu.epam.fop.lambdas.calculator;

import edu.epam.fop.lambdas.insurance.Accommodation;
import edu.epam.fop.lambdas.insurance.Currency;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Optional;

public final class AccommodationInsurancePolicies {

    private AccommodationInsurancePolicies() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    static InsuranceCalculator<Accommodation> rentDependentInsurance(BigInteger divider) {
        if (divider == null) {
            throw new NullPointerException();
        }

        return accommodation -> {
            if (
                    accommodation == null
                            || accommodation.rent().isEmpty()
                            || accommodation.rent().get().currency() != Currency.USD
                            || accommodation.rent().get().amount().compareTo(BigInteger.ZERO) <= 0
                            || accommodation.rent().get().unit().getMonths() == 0
            ) {
                return Optional.empty();
            }

            BigDecimal rentAmount = new BigDecimal(accommodation.rent().get().amount());
            BigInteger insuranceCoefficient = rentAmount.divide(new BigDecimal(divider), 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)).toBigInteger();

            if (insuranceCoefficient.compareTo(BigInteger.valueOf(100)) > 0) {
                return Optional.of(InsuranceCoefficient.MAX);
            } else if (insuranceCoefficient.equals(BigInteger.ZERO)) {
                return Optional.empty();
            } else {
                return Optional.of(InsuranceCoefficient.of(insuranceCoefficient));
            }
        };
    }

    static InsuranceCalculator<Accommodation> priceAndRoomsAndAreaDependentInsurance(
            BigInteger priceThreshold,
            int roomsThreshold,
            BigInteger areaThreshold
    ) {
        if (priceThreshold == null || areaThreshold == null) {
            throw new NullPointerException();
        }

        return accomodation -> {
            if (
                    accomodation == null
                    || accomodation.price() == null
                    || accomodation.rooms() == null
                    || accomodation.area() == null
            ) {
                return Optional.of(InsuranceCoefficient.of(0));
            }

            if (
                    accomodation.price().compareTo(priceThreshold) >= 0
                    && accomodation.rooms().compareTo(roomsThreshold) >= 0
                    && accomodation.area().compareTo(areaThreshold) >= 0
            ) {
                return Optional.of(InsuranceCoefficient.MAX);
            }

            return Optional.of(InsuranceCoefficient.MIN);
        };
    }
}
