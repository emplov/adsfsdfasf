package edu.epam.fop.lambdas.calculator;

import edu.epam.fop.lambdas.insurance.Car;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

public final class CarInsurancePolicies {

    private CarInsurancePolicies() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static InsuranceCalculator<Car> ageDependInsurance(LocalDate baseDate) {
        if (baseDate == null) {
            throw new NullPointerException();
        }

        return car -> {
            if (car == null) {
                return Optional.empty();
            }

            LocalDate manufacturedDate = car.manufactureDate();

            if (manufacturedDate == null) {
                return Optional.empty();
            }

            Period age = Period.between(manufacturedDate, baseDate);

            if (age.getYears() <= 1) {
                return Optional.of(InsuranceCoefficient.MAX);
            } else if (age.getYears() <= 5) {
                return Optional.of(InsuranceCoefficient.of(70));
            } else if (age.getYears() <= 10) {
                return Optional.of(InsuranceCoefficient.of(30));
            } else {
                return Optional.of(InsuranceCoefficient.MIN);
            }
        };
    }

    public static InsuranceCalculator<Car> priceAndOwningOfFreshCarInsurance(
            LocalDate baseDate,
            BigInteger priceThreshold,
            Period owningThreshold
    ) {
        if (baseDate == null || owningThreshold == null || priceThreshold == null) {
            throw new NullPointerException();
        }

        return car -> {
            if (
                    car == null
                    || car.price() == null
                    || car.purchaseDate() == null
                    || car.soldDate().isPresent()
            ) {
                return Optional.empty();
            }

            LocalDate purchaseDate = car.purchaseDate();
            if (car.price().compareTo(priceThreshold) < 0) {
                return Optional.empty(); // car price is below threshold
            }
            if (purchaseDate.plus(owningThreshold).isAfter(baseDate)) {
                return Optional.empty(); // car has not been owned for long enough
            }
            BigDecimal price = new BigDecimal(car.price());
            BigDecimal threshold = new BigDecimal(priceThreshold);
            if (price.compareTo(threshold.multiply(BigDecimal.valueOf(3))) >= 0) {
                return Optional.of(InsuranceCoefficient.MAX);
            } else if (price.compareTo(threshold.multiply(BigDecimal.valueOf(2))) >= 0) {
                return Optional.of(InsuranceCoefficient.of(50));
            } else {
                return Optional.of(InsuranceCoefficient.MIN);
            }
        };
    }
}
